﻿namespace first_impression.Contracts
{
    public class UserAuthDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
