﻿namespace first_impression.Contracts
{
    public class PasswordResetRequestDto
    {
        public string Email { get; set; }
        public string ChangePasswordRoute { get; set; }
    }
}
