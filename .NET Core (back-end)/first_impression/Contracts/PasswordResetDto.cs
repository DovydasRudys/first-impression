﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace first_impression.Contracts
{
    public class PasswordResetDto
    {
        public string Id { get; set; }
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}
