﻿namespace first_impression.Contracts
{
    public class PasswordResetTokenValidationDto
    {
        public string Id { get; set; }
        public string Token { get; set; }
    }
}
