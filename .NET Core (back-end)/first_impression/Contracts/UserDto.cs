﻿using first_impression.Entities;

namespace first_impression.Contracts
{
    public class UserDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string PictureUrl { get; set; }
    }
}
