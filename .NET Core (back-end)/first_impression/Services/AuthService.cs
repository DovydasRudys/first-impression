﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using first_impression.Entities;
using first_impression.ServiceInterfaces;
using System;
using System.Threading.Tasks;

namespace first_impression.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUsersService usersService;
        private readonly IConfiguration configuration;
        private readonly UserManager<User> userManager;
        private readonly IEmailSender emailSender;

        public AuthService(IUsersService usersService, IConfiguration configuration, UserManager<User> userManager, IEmailSender emailSender)
        {
            this.usersService = usersService;
            this.configuration = configuration;
            this.userManager = userManager;
            this.emailSender = emailSender;
        }

        public async Task<IActionResult> AuthenticateUser(string email, string password)
        {
            var user = await usersService.AuthenticateCredentials(email, password);

            if (user == null)
                throw new ArgumentException("Invalid email and/or password...");

            string secret = Environment.GetEnvironmentVariable("SECRET") ?? configuration["Secret"];

            var token = JWTokenGenerator.GenerateToken(user, configuration["Secret"], TimeSpan.FromDays(7));

            var authDto = usersService.GetUserDto(user);

            return new OkObjectResult( new { userData = authDto, token });
        }

        public async Task SendPasswordResetRequest(User user, string changePasswordRoute)
        {
            if (user == null)
                throw new ArgumentNullException();

            string origin = Environment.GetEnvironmentVariable("ALLOW_ORIGIN") ?? configuration["Origin"];

            var token = await userManager.GeneratePasswordResetTokenAsync(user);

            var resetUrl = EmailHelper.GetPasswordResetLink(user, token, origin, changePasswordRoute);

            var message = EmailHelper.GetPasswordResetMessage(user, resetUrl);

            await emailSender.SendEmailAsync(user.Email, "Password reset requested", message);
        }

        public async Task<bool> VerifyPasswordResetToken(User user, string token)
        {
            if (user == null)
                throw new ArgumentNullException();

            var result = await userManager.VerifyUserTokenAsync(user, "Default", UserManager<User>.ResetPasswordTokenPurpose, token);

            if (result)
                return true;

            return false;
        }

        public async Task<bool> TryChangePassword(User user, string token, string newPassword)
        {
            if (user == null)
                throw new ArgumentNullException();

            var result = await userManager.ResetPasswordAsync(user, token, newPassword);

            if (result.Succeeded)
            {
                usersService.UpdateUserAccessKey(user);

                return true;
            }

            return false;
        }
    }
}
