﻿using System.Linq;
using first_impression.Entities;
using first_impression.Contexts;
using Microsoft.AspNetCore.Identity;
using first_impression.Contracts;
using System.Threading.Tasks;
using System;
using first_impression.Validators;
using first_impression.ServiceInterfaces;

namespace first_impression.Services
{
    public class UsersService : PasswordHasher<User>, IUsersService
    {
        private AppDatabaseContext context;
        private SignInManager<User> signInManager;

        public UsersService(AppDatabaseContext context, SignInManager<User> signInManager)
        {
            this.context = context;
            this.signInManager = signInManager;
        }

        public bool ValidateData(UserRegisterDto userDto)
        {
            var validator = new RegisterValidator();
            return validator.Validate(userDto).IsValid;
        }

        private User GetUserData(UserRegisterDto userDto)
        {
            return new User()
            {
                Email = userDto.Email,
                Name = userDto.Name,
                PictureUrl = userDto.PictureUrl
            };
        }

        public User Create(UserRegisterDto userDto)
        {
            if (context.Users.Any(x => x.Email == userDto.Email))
                return null;

            User user = GetUserData(userDto);

            user.PasswordHash = HashPassword(user, userDto.Password);

            user.SecurityStamp = Guid.NewGuid().ToString();
            user.AccessKey = Guid.NewGuid().ToString();
            user.UserName = user.Email;

            context.Users.Add(user);
            context.SaveChanges();

            return user;
        }

        public void Delete(string id)
        {
            User userToDelete = context.Users.FirstOrDefault(user => string.Compare(user.Id, id) == 0);

            if (userToDelete == null)
                throw new NullReferenceException("There is no user with such id");

            context.Users.Remove(userToDelete);
            context.SaveChanges();
        }
        
        public async Task<User> AuthenticateCredentials(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                return null;

            var user = context.Users.SingleOrDefault(x => x.Email == email);

            if (user != null)
            {
                var result = await signInManager.CheckPasswordSignInAsync(user, password, false);

                if (result.Succeeded)
                    return user;
            }

            return null;
        }

        public void UpdateUserAccessKey(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.AccessKey = Guid.NewGuid().ToString();

            context.SaveChanges();
        }
        
        public User GetByEmail(string email)
        {
            return context.Users.Where(x => x.Email == email).SingleOrDefault();
        }

        public User GetById(string id)
        {
            return context.Users.Find(id);
        }

        public UserDto GetUserDto(User user)
        {
            if (user == null)
                throw new ArgumentNullException();

            return new UserDto()
            {
                Id = user.Id,
                Email = user.Email,
                Name = user.Name,
                PictureUrl = user.PictureUrl
            };
        }

        public UserDto GetNextStranger(string myId)
        {
            User stranger = context.Users.FirstOrDefault(user => context.Impressions.Where(impression => impression.SenderId == myId && impression.ReceiverId == user.Id).Count() == 0 && user.Id != myId);
            if (stranger != null)
                return GetUserDto(stranger);
            throw new Exception("No eligible strangers found");
        }
    }
}
