﻿using first_impression.Entities;
using first_impression.Contexts;
using first_impression.Contracts;
using System;
using System.Linq;
using System.Collections.Generic;
using first_impression.ServiceInterfaces;

namespace first_impression.Services
{
    public class ImpressionService : IImpressionService
    {
        private AppDatabaseContext context;

        public ImpressionService(AppDatabaseContext context)
        {
            this.context = context;
        }

        public void Create(Impression impression)
        {
            context.Impressions.Add(impression);
            context.SaveChanges();
        }

        public List<Impression> GetImpressionsOnUser(string id)
        {
            try
            {
                List<Impression> usersImpressions = context.Impressions.Where(impression => impression.ReceiverId == id).ToList();
                return usersImpressions;
            }
            catch
            {
                throw new ArgumentException("No impressions on this user");
            }
        }

        public string GetMostFrequentName(string id)
        {
            try
            {
                return context.Impressions.Where(impression => impression.ReceiverId == id).GroupBy(impression => impression.GuessedName).OrderByDescending(group => group.Count()).FirstOrDefault().Key;
            }
            catch
            {
                throw new ArgumentException("No impressions on this user");
            }
        }

        public string GetMostFrequentJob(string id)
        {
            try
            {
                return context.Impressions.Where(impression => impression.ReceiverId == id).GroupBy(impression => impression.GuessedJob).OrderByDescending(group => group.Count()).FirstOrDefault().Key;
            }
            catch
            {
                throw new ArgumentException("No impressions on this user");
            }
        }
    }
}
