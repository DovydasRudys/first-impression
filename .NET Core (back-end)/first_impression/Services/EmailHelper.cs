﻿using first_impression.Entities;
using System;
using System.Web;

namespace first_impression.Services
{
    public static class EmailHelper
    {
        public static readonly string PasswordResetEmailTemplate = "<body style=\"margin: 0; padding: 0;\"> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"650\" style=\"border-collapse: collapse\" > <tr> <td style=\"text-align: center\"> <img src=\"https://media.licdn.com/dms/image/C560BAQH32zNds7mloQ/company-logo_200_200/0?e=2159024400&amp;v=beta&amp;t=H9s6cXo2d5cgIsBpnwD49yAXkbjmTDxqJev2C4n1BjI\" alt=\"logo\" width=\"100px\" height=\"100px\" style=\"align-self: center; padding-top: 5px\"/> <p style=\"color: black; font-size: 30px; padding-top: 20px; font-family: Helvetica, sans-serif;\" > <strong>Hello {0},</strong> </p></td></tr><tr> <td> <p style=\"padding-top: 10px; padding-left: 10px; padding-bottom: 10px; text-align: left; color: black; font-size: 18px;\" > A request has been made to reset your password. If you wish to reset your password, click the button below: </p></td></tr><tr> <td style=\"text-align: center\"> <p> <a href=\"{1}\" style=\"background: #ED2939;font-size: 16px;padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;\" >Reset password</a > </p></td></tr><tr> <td> <p style=\"color: black; font-size: 14px; padding-left: 10px\"> Or paste this link into your browser's search bar: <br/> <a href=\"{1}\" style=\"font-size: 12px\">{1}</a> </p></td></tr><tr> <td> <p style=\"color: gray; font-size: 12px; padding-left: 10px\"> If you didn't make this request, ignore this email. </p></td></tr></table></body>";

        public static string GetPasswordResetLink(User user, string token, string originUrl, string changePasswordRoute)
        {
            if (user == null)
                throw new ArgumentNullException();

            var encodedToken = HttpUtility.UrlEncode(token);

            return $"{originUrl}{changePasswordRoute}?id={user.Id}&token={encodedToken}";
        }

        public static string GetPasswordResetMessage(User user, string resetUrl)
        {
            if (user == null)
                throw new ArgumentNullException();

            return string.Format(PasswordResetEmailTemplate, user.Name, resetUrl);
        }
    }
}
