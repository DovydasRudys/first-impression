﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

namespace first_impression.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly IConfiguration configuration;

        public EmailSender(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            string sendGridKey = Environment.GetEnvironmentVariable("SEND_GRID_KEY") ?? configuration["SendGridKey"];
            string senderEmail = Environment.GetEnvironmentVariable("SENDER_EMAIL") ?? configuration["SenderEmail"];
            string senderName = Environment.GetEnvironmentVariable("SENDER_NAME") ?? configuration["SenderName"];

            var client = new SendGridClient(sendGridKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(senderEmail, senderName),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };

            msg.AddTo(new EmailAddress(email));

            msg.SetClickTracking(false, false);

            return client.SendEmailAsync(msg);
        }
    }
}
