﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace first_impression.Entities
{
    public class Impression
    {
        public string Id { get; set; }
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public string GuessedName { get; set; }
        public string GuessedJob { get; set; }
        public bool WouldBeFriends { get; set; }
        public string WhyWouldBeFriends { get; set; }
    }
}
