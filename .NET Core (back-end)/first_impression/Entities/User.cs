﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace first_impression.Entities
{
    public enum Roles { User, Manager, Admin }

    public class User : IdentityUser
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string PictureUrl { get; set; }
        [Required]
        public string AccessKey { get; set; }
    }
}
