﻿using FluentValidation;
using first_impression.Contracts;

namespace first_impression.Validators
{
    public class RegisterValidator : AbstractValidator<UserRegisterDto>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.Password).Length(8, 60).Matches(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]");
            RuleFor(x => x.Email).EmailAddress().MaximumLength(60);
            RuleFor(x => x.Name).Length(1, 50);
        }
    }
}
