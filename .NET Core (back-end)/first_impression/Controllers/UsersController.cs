﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using first_impression.Contracts;
using first_impression.Entities;
using first_impression.ServiceInterfaces;

namespace first_impression.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUsersService usersService;

        public UsersController(IUsersService usersService)
        {
            this.usersService = usersService;
        }

        // POST: api/Users
        [AllowAnonymous]
        [HttpPost("register")]
        public ActionResult Register([FromBody]UserRegisterDto userDto)
        {
            if(!usersService.ValidateData(userDto))
                return BadRequest("Invalid data entered");

            var user = usersService.Create(userDto);

            if (user == null)
                return Ok("A user with this email already exists");

            return StatusCode(201, "Registration successful");
        }

        // GET api/users/{userId}
        [Authorize]
        [HttpGet("{id}")]
        public ActionResult<UserDto> GetStranger([FromRoute] string id)
        {
            return Ok(usersService.GetNextStranger(id));
        }

        // DELETE api/users/5
        [Authorize(Roles ="Admin")]
        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] string id)
        {
            try
            {
                usersService.Delete(id);
                return NoContent();
            }
            catch (NullReferenceException error)
            {
                return NotFound(error.Message);
            }
        }
    }
}
