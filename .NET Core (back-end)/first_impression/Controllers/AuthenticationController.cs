﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using first_impression.Contracts;
using first_impression.ServiceInterfaces;

namespace first_impression.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthService authService;
        private readonly IUsersService usersService;

        public AuthenticationController(IAuthService authService, IUsersService usersService)
        {
            this.authService = authService;
            this.usersService = usersService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody]UserAuthDto userDto)
        {
            try
            {
                return await authService.AuthenticateUser(userDto.Email, userDto.Password);
            }
            catch (ArgumentException ex)
            {
                return StatusCode((int)HttpStatusCode.Unauthorized, ex.Message);
            }
        }

        [Authorize]
        [HttpGet("check-authentication")]
        public ActionResult<UserDto> CheckAuthentication()
        {
            try
            {
                return usersService.GetUserDto(usersService.GetById(User.FindFirst("Id")?.Value));
            }
            catch (ArgumentNullException ex)
            {
                return Unauthorized();
            }
        }

        [AllowAnonymous]
        [HttpPost("request-password-reset")]
        public async Task<IActionResult> RequestPasswordRequest([FromBody]PasswordResetRequestDto dto)
        {
            try
            {
                await authService.SendPasswordResetRequest(usersService.GetByEmail(dto.Email), dto.ChangePasswordRoute);
            }
            catch { } //For security reasons, client is not informed about failure to send password request emails.

            return Accepted();
        }

        [AllowAnonymous]
        [HttpPost("validate-password-reset-token")]
        public async Task<IActionResult> ValidatePasswordResetToken([FromBody]PasswordResetTokenValidationDto dto)
        {
            if (await authService.VerifyPasswordResetToken(usersService.GetById(dto.Id), dto.Token))
                return Ok();

            return StatusCode((int)HttpStatusCode.Unauthorized, "Password reset token is either invalid or expired");
        }

        [AllowAnonymous]
        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody]PasswordResetDto dto)
        {
            if (await authService.TryChangePassword(usersService.GetById(dto.Id), dto.Token, dto.NewPassword))
                return Ok("Password changed successfully!");

            return BadRequest("Failed to change password");
        }
    }
}