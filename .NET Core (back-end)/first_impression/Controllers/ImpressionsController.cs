﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using first_impression.Contracts;
using first_impression.ServiceInterfaces;
using first_impression.Entities;
using Hangfire;

namespace first_impression.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImpressionsController : ControllerBase
    {
        private IImpressionService impressionService;
        private IUsersService usersService;

        public ImpressionsController(IImpressionService impressionService, IUsersService usersService)
        {
            this.impressionService = impressionService;
            this.usersService = usersService;

        }

        [HttpGet]
        public ActionResult GetUserImpressions()
        {
            return new OkObjectResult(new { text = "It works!" });
        }

        //GET api/impressions/{userid}
        [Authorize]
        [HttpGet("{userId}")]
        public ActionResult GetUserImpressions([FromRoute] string userId)
        {
            List<Impression> impressionsAboutUser = impressionService.GetImpressionsOnUser(userId);
            return Ok(impressionsAboutUser);
        }

        //GET api/impressions/{userid}/name
        [Authorize]
        [HttpGet("{userId}/name")]
        public ActionResult GetMostFrequentName([FromRoute] string userId)
        {
            string name = impressionService.GetMostFrequentName(userId);
            return Ok(name);
        }

        //GET api/impressions/{userid}/job
        [Authorize]
        [HttpGet("{userId}/job")]
        public ActionResult GetMostFrequentJob([FromRoute] string userId)
        {
            string job = impressionService.GetMostFrequentJob(userId);
            return Ok(job);
        }

        // POST api/impressions
        [Authorize]
        [HttpPost]
        public ActionResult CreateNewImpression([FromBody] Impression impression)
        {
            try
            {
                impressionService.Create(impression);

                return Ok();
            }
            catch (ArgumentException error)
            {
                return BadRequest(error.Message);
            }
        }
    }
}