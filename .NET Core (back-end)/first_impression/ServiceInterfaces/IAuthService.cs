﻿using Microsoft.AspNetCore.Mvc;
using first_impression.Entities;
using System.Threading.Tasks;

namespace first_impression.ServiceInterfaces
{
    public interface IAuthService
    {
        Task<IActionResult> AuthenticateUser(string email, string password);
        Task SendPasswordResetRequest(User user, string changePasswordRoute);
        Task<bool> VerifyPasswordResetToken(User user, string token);
        Task<bool> TryChangePassword(User user, string token, string newPassword);
    }
}
