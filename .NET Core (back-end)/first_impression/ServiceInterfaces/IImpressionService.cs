﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using first_impression.Entities;

namespace first_impression.ServiceInterfaces
{
    public interface IImpressionService
    {
        void Create(Impression impression);
        List<Impression> GetImpressionsOnUser(string id);
        string GetMostFrequentName(string id);
        string GetMostFrequentJob(string id);
    }
}
