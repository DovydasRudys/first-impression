﻿using first_impression.Contracts;
using first_impression.Entities;
using System.Threading.Tasks;

namespace first_impression.ServiceInterfaces
{
    public interface IUsersService
    {
        void Delete(string id);
        User Create(UserRegisterDto userDto);
        Task<User> AuthenticateCredentials(string email, string password);
        void UpdateUserAccessKey(User user);
        User GetByEmail(string email);
        User GetById(string id);
        UserDto GetUserDto(User user);
        bool ValidateData(UserRegisterDto userDto);
        UserDto GetNextStranger(string myId);
    }
}
