package com.example.firstimpression;

import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ImgurAPI {

    @Multipart
    @POST("3/image")
    Call<JsonObject> postImage(@Header("Authorization") String auth, @Part MultipartBody.Part image);
}
