package com.example.firstimpression;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Statistics extends Activity {

    private ViewDialog viewDialog;
    private List<JsonObject> impressionsOnMe;
    TextView mostFrequentName;
    TextView mostFrequentJob;
    TextView friendsPercentage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        viewDialog = new ViewDialog(this);
        impressionsOnMe = new ArrayList<JsonObject>();

        mostFrequentName = findViewById(R.id.mostFrequentName);
        mostFrequentName.setText("Nobody guessed yet");

        mostFrequentJob = findViewById(R.id.mostFrequentJob);
        mostFrequentJob.setText("Nobody guessed yet");

        friendsPercentage = findViewById(R.id.friendsPercentage);
        friendsPercentage.setText("0-100%");

        fetchImpressions();

        CardView homeButton = findViewById(R.id.homeButton);
        CardView commentsButton = findViewById(R.id.commentsButton);

        homeButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(Statistics.this, MainActivity.class));
            }
        });

        commentsButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(Statistics.this, Comments.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    protected void fetchImpressions() {
        viewDialog.showDialog();

        String myId = ((FirstImpression) getApplication()).getCurrentUser().Id;
        String authToken = ((FirstImpression) getApplication()).getToken();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        //Call<AuthenticationResponse> call = service.authenticate(new LoginUser(email.getText().toString(), password.getText().toString()));
        Call<List<JsonObject>> call = service.getImpressionsOnUser(myId, "Bearer " + authToken);
        //Execute the request asynchronously//
        call.enqueue(new Callback<List<JsonObject>>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<List<JsonObject>> call, Response<List<JsonObject>> response) {
                int code = response.code();

                if(code == 200) {
                    impressionsOnMe = response.body();
                    if(impressionsOnMe.size() > 0){
                        mostFrequentName();
                        mostFrequentJob();
                        friendsPercentage(impressionsOnMe);
                    }
                }
                else{
                    Toast.makeText(Statistics.this, "Error ("+String.valueOf(code)+")", Toast.LENGTH_SHORT).show();
                }
                viewDialog.hideDialog();
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<List<JsonObject>> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(Statistics.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void mostFrequentName(){
        viewDialog.showDialog();
        String myId = ((FirstImpression) getApplication()).getCurrentUser().Id;
        String authToken = ((FirstImpression) getApplication()).getToken();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        //Call<AuthenticationResponse> call = service.authenticate(new LoginUser(email.getText().toString(), password.getText().toString()));
        Call<String> call = service.getName(myId, "Bearer " + authToken);
        //Execute the request asynchronously//
        call.enqueue(new Callback<String>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<String> call, Response<String> response) {
                int code = response.code();

                if(code == 200) {
                    mostFrequentName.setText(response.body());
                }
                else{
                    Toast.makeText(Statistics.this, "Error ("+String.valueOf(code)+")", Toast.LENGTH_SHORT).show();
                }
                viewDialog.hideDialog();
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<String> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(Statistics.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void mostFrequentJob(){
        viewDialog.showDialog();
        String myId = ((FirstImpression) getApplication()).getCurrentUser().Id;
        String authToken = ((FirstImpression) getApplication()).getToken();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        //Call<AuthenticationResponse> call = service.authenticate(new LoginUser(email.getText().toString(), password.getText().toString()));
        Call<String> call = service.getJob(myId, "Bearer " + authToken);
        //Execute the request asynchronously//
        call.enqueue(new Callback<String>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<String> call, Response<String> response) {
                int code = response.code();

                if(code == 200) {
                    mostFrequentJob.setText(response.body());
                }
                else{
                    Toast.makeText(Statistics.this, "Error ("+String.valueOf(code)+")", Toast.LENGTH_SHORT).show();
                }
                viewDialog.hideDialog();
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<String> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(Statistics.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void friendsPercentage(List<JsonObject> impressions){
        int count = 0;

        for (JsonObject impression : impressions) {
            if(impression.get("wouldBeFriends").getAsBoolean())
                count++;
        }

        friendsPercentage.setText(Math.round((float)count / impressions.size() * 100) + "%");
    }
}
