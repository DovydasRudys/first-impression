package com.example.firstimpression;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    public String Id;
    @SerializedName("email")
    public String Email;
    @SerializedName("name")
    public String Name;
    @SerializedName("pictureUrl")
    public String PictureUrl;

    public User(String id, String email, String name, String pictureUrl) {
        this.Id = id;
        this.Email = email;
        this.Name = name;
        this.PictureUrl = pictureUrl;
    }
}