package com.example.firstimpression;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImgurClient {

    private static Retrofit retrofit;

    //Define the base URL//
    private static final String IMG_URL = "https://api.imgur.com/";

    //Create the Retrofit instance//
    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(IMG_URL)

                    //Add the converter//
                    .addConverterFactory(GsonConverterFactory.create(gson))

                    //Build the Retrofit instance//
                    .build();
        }
        return retrofit;
    }
}