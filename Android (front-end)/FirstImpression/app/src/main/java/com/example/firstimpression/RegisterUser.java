package com.example.firstimpression;

import com.google.gson.annotations.SerializedName;

public class RegisterUser {
    @SerializedName("email")
    public String Email;
    @SerializedName("password")
    public String Password;
    @SerializedName("name")
    public String Name;
    @SerializedName("pictureUrl")
    public String PictureUrl;

    public RegisterUser(String email, String password, String name, String photoUrl){
        this.Email = email;
        this.Password = password;
        this.Name = name;
        this.PictureUrl = photoUrl;
    }
}
