package com.example.firstimpression;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.example.firstimpression.Utils.getBytes;

public class RegisterActivity extends Activity {

    public static final int REQUEST_CODE_PICK_IMAGE = 1001;
    public static final int REQUEST_STORAGE_PERMISSION = 1002;

    ViewDialog viewDialog;
    ImageView uploadImage;
    String imgURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        viewDialog = new ViewDialog(this);
        imgURL = "";

        CardView registerButton = findViewById(R.id.registerButton);
        uploadImage = findViewById(R.id.imageView);

        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
            return;
        }

        registerButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {

                    tryRegister(imgURL);
            }
        });
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            Toast.makeText(RegisterActivity.this, "You must choose a picture.", Toast.LENGTH_LONG).show();
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_PICK_IMAGE && null != data) {
            try {
                InputStream is = getContentResolver().openInputStream(data.getData());

                Picasso.get().load(data.getData()).into(uploadImage);

                uploadImage(getBytes(is));

            } catch (IOException e) {
                Toast.makeText(RegisterActivity.this, "There was a problem with picture upload.", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void tryRegister(String pictureUrl) {
        String email = ((EditText) findViewById(R.id.email)).getText().toString();
        String password = ((EditText) findViewById(R.id.password)).getText().toString();
        String repeatPassword = ((EditText) findViewById(R.id.repeatPassword)).getText().toString();
        String name = ((EditText) findViewById(R.id.name)).getText().toString();

        if(!Utils.isValidEmail(email)){
            Toast.makeText(RegisterActivity.this, "Incorrect email format.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!Utils.isValidPassword(password)){
            Toast.makeText(RegisterActivity.this, "Password must be at least 8 characters in length and contain lowercase and uppercase letters and at least one number.", Toast.LENGTH_LONG).show();
            return;
        }

        if(!password.equals(repeatPassword)){
            Toast.makeText(RegisterActivity.this, "Passwords don't match.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(pictureUrl.isEmpty() || pictureUrl.equals(null)) {
            Toast.makeText(RegisterActivity.this, "Failed to upload picture", Toast.LENGTH_SHORT).show();
            return;
        }

        viewDialog.showDialog();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        //Call<AuthenticationResponse> call = service.authenticate(new LoginUser(email.getText().toString(), password.getText().toString()));
        Call<String> call = service.register(new RegisterUser(email, password, name, pictureUrl));
        //Execute the request asynchronously//
        call.enqueue(new Callback<String>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<String> call, Response<String> response) {
                viewDialog.hideDialog();
                int code = response.code();

                if(code == 201) {
                    Toast.makeText(RegisterActivity.this, "Registration successful !", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                }
                else{
                    Toast.makeText(RegisterActivity.this, "This email address is already taken.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<String> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(RegisterActivity.this, "We apologize for inconvenience. Server is down.", Toast.LENGTH_LONG).show();
            }
        });
    }

    protected void uploadImage(byte[] imageBytes) {
        viewDialog.showDialog();
        //Create a handler for the RetrofitInstance interface//
        ImgurAPI service = ImgurClient.getRetrofitInstance().create(ImgurAPI.class);

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageBytes);

        MultipartBody.Part body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);

        Call<JsonObject> call = service.postImage(Constants.getClientAuth(), body);
        //Execute the request asynchronously//
        call.enqueue(new Callback<JsonObject>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.code() != 200) {
                        imgURL = "";
                    } else {
                        imgURL = response.body().getAsJsonObject("data").get("link").getAsString();
                    }
                }
                catch (Exception ex){
                    Toast.makeText(RegisterActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
                }
                viewDialog.hideDialog();
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<JsonObject> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(RegisterActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                imgURL = "";
            }
        });
    }
}
