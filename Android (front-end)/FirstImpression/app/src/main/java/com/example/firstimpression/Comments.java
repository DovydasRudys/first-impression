package com.example.firstimpression;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Comments extends Activity {

    private ViewDialog viewDialog;

    ArrayList<String> dataModels;
    ListView listView;
    private static CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);


        listView=findViewById(R.id.list);

        viewDialog = new ViewDialog(this);

        fetchComments();

        ImageView homeButton = findViewById(R.id.homeButton);

        homeButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(Comments.this, MainActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    protected void fetchComments() {
        viewDialog.showDialog();

        String myId = ((FirstImpression) getApplication()).getCurrentUser().Id;
        String authToken = ((FirstImpression) getApplication()).getToken();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        dataModels= new ArrayList<>();

        Call<List<JsonObject>> call = service.getImpressionsOnUser(myId, "Bearer " + authToken);
        //Execute the request asynchronously//
        call.enqueue(new Callback<List<JsonObject>>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<List<JsonObject>> call, Response<List<JsonObject>> response) {
                int code = response.code();

                if(code == 200) {
                    for (JsonObject impression : response.body()) {
                        if (impression.get("whyWouldBeFriends").getAsString().length() > 0) {
                            dataModels.add(impression.get("whyWouldBeFriends").getAsString());
                        }
                    }

                    if(dataModels.size() == 0) {
                        dataModels.add("There are no comments yet");
                    }

                    adapter= new CustomAdapter(dataModels,getApplicationContext());
                    listView.setAdapter(adapter);
                }
                else{
                    Log.d("comment:", String.valueOf(code));
                    Toast.makeText(Comments.this, "Error ("+String.valueOf(code)+")", Toast.LENGTH_SHORT).show();
                }
                viewDialog.hideDialog();
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<List<JsonObject>> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(Comments.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
