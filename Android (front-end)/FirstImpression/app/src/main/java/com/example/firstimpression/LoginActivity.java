package com.example.firstimpression;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.firstimpression.Utils.isNetworkAvailable;

public class LoginActivity extends Activity {
    ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        //..initialize our custom loading dialog here with passing this activity context
        viewDialog = new ViewDialog(this);

        CardView loginButton = findViewById(R.id.loginButton);
        TextView registerButton = findViewById(R.id.registerButton);

        loginButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                tryToLogin();
            }
        });


        registerButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        if(!isNetworkAvailable(this)) {
            Toast.makeText(this,"Internet connection is required.",Toast.LENGTH_SHORT).show();
            finish(); //Calling this method to close this activity when internet is not available.
        }
    }

    protected void tryToLogin() {
        EditText email = findViewById(R.id.editText);
        if(!Utils.isValidEmail(email.getText().toString())){
            Toast.makeText(LoginActivity.this, "Incorrect email format.", Toast.LENGTH_LONG).show();
            return;
        }

        EditText password = findViewById(R.id.editText2);
        if(!Utils.isValidPassword(password.getText().toString())){
            Toast.makeText(LoginActivity.this, "Password must be at least 8 characters in length and contain lowercase and uppercase letters and at least one number.", Toast.LENGTH_LONG).show();
            return;
        }

        viewDialog.showDialog();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        Call<AuthenticationResponse> call = service.authenticate(new LoginUser(email.getText().toString(), password.getText().toString()));
        //Call<AuthenticationResponse> call = service.authenticate(new LoginUser("admin@gmail.com", "Administratorius1"));
        //Execute the request asynchronously//
        call.enqueue(new Callback<AuthenticationResponse>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                viewDialog.hideDialog();
                int code = response.code();

                if(code == 200) {
                    ((FirstImpression) getApplication()).setCurrentUser(response.body().userData);
                    ((FirstImpression) getApplication()).setToken(response.body().token);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }
                else{
                    Toast.makeText(LoginActivity.this, "We couldn't find user with these credentials.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<AuthenticationResponse> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(LoginActivity.this, "We apologize for inconvenience. Server is down.", Toast.LENGTH_LONG).show();
            }
        });
    }
}
