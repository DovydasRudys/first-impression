package com.example.firstimpression;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface API {

    //Specify the request type and pass the relative URL//

    //Wrap the response in a Call object with the type of the expected result//
    @POST("api/authentication")
    Call<AuthenticationResponse> authenticate(@Body LoginUser loginUser);

    @POST("api/users/register")
    Call<String> register(@Body RegisterUser registerUser);

    @GET("api/impressions")
    Call<String> test();

    @GET("api/users/{id}")
    Call<User> getStranger(@Path("id") String id, @Header("Authorization") String authHeader);

    @POST("api/impressions")
    Call<Void> postImpression(@Body JsonObject impression, @Header("Authorization") String authHeader);

    @GET("api/impressions/{userId}")
    Call<List<JsonObject>> getImpressionsOnUser(@Path("userId") String userId, @Header("Authorization") String authHeader);

    @GET("api/impressions/{userId}/name")
    Call<String> getName(@Path("userId") String userId, @Header("Authorization") String authHeader);

    @GET("api/impressions/{userId}/job")
    Call<String> getJob(@Path("userId") String userId, @Header("Authorization") String authHeader);
}