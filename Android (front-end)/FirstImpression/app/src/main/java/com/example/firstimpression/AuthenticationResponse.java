package com.example.firstimpression;

import com.google.gson.annotations.SerializedName;

public class AuthenticationResponse {
    public User userData;
    public String token;

    public AuthenticationResponse(User userData, String token){
        this.userData = userData;
        this.token = token;
    }
}
