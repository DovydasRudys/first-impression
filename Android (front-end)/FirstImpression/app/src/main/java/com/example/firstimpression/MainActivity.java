package com.example.firstimpression;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    User stranger;
    ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewDialog = new ViewDialog(this);

        loadNextStranger();

        CardView statisticsButton = findViewById(R.id.statisticsButton);
        CardView submitButton = findViewById(R.id.submitButton);

        statisticsButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(MainActivity.this, Statistics.class));
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                tryPostImpression();
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    protected void tryPostImpression() {
        String name = ((EditText) findViewById(R.id.name)).getText().toString();
        String employment = ((EditText) findViewById(R.id.employment)).getText().toString();
        Boolean friends = ((CheckBox) findViewById(R.id.friends)).isChecked();
        String why = ((EditText) findViewById(R.id.why)).getText().toString();

        if(name.isEmpty() || employment.isEmpty() || why.isEmpty()){
            Toast.makeText(MainActivity.this, "All fields must be filled in.", Toast.LENGTH_SHORT).show();
            return;
        }

        viewDialog.showDialog();
        String myId = ((FirstImpression) getApplication()).getCurrentUser().Id;
        String authToken = ((FirstImpression) getApplication()).getToken();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        JsonObject impression = new JsonObject();
        impression.addProperty("senderId", myId);
        impression.addProperty("receiverId", stranger.Id);
        impression.addProperty("guessedName", name);
        impression.addProperty("guessedJob", employment);
        impression.addProperty("wouldBeFriends", friends);
        impression.addProperty("whyWouldBeFriends", why);

        //Call<AuthenticationResponse> call = service.authenticate(new LoginUser(email.getText().toString(), password.getText().toString()));
        Call<Void> call = service.postImpression(impression, "Bearer " + authToken);
        //Execute the request asynchronously//
        call.enqueue(new Callback<Void>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<Void> call, Response<Void> response) {
                int code = response.code();

                if(code == 200) {
                    loadNextStranger();
                }
                else{
                    Toast.makeText(MainActivity.this, "We apologize. Server is down.", Toast.LENGTH_SHORT).show();
                }
                viewDialog.hideDialog();
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<Void> call, Throwable throwable) {
                viewDialog.hideDialog();
                Toast.makeText(MainActivity.this, "We apologize. Server is down.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void loadNextStranger() {
        viewDialog.showDialog();
        String myId = ((FirstImpression) getApplication()).getCurrentUser().Id;
        String authToken = ((FirstImpression) getApplication()).getToken();
        //Create a handler for the RetrofitInstance interface//
        API service = RetrofitClient.getRetrofitInstance().create(API.class);

        //Call<AuthenticationResponse> call = service.authenticate(new LoginUser(email.getText().toString(), password.getText().toString()));
        Call<User> call = service.getStranger(myId, "Bearer " + authToken);
        //Execute the request asynchronously//
        call.enqueue(new Callback<User>() {
            @Override
            //Handle a successful response//
            public void onResponse(Call<User> call, Response<User> response) {
                int code = response.code();

                if(code == 200) {
                    stranger = response.body();
                    ImageView image = findViewById(R.id.imageView);
                    Picasso.get().load(stranger.PictureUrl).into(image);

                    ((EditText) findViewById(R.id.name)).setText("");
                    ((EditText) findViewById(R.id.employment)).setText("");
                    ((CheckBox) findViewById(R.id.friends)).setChecked(false);
                    ((EditText) findViewById(R.id.why)).setText("");
                }
                else{
                    startActivity(new Intent(MainActivity.this, NobodyNew.class));
                }
                viewDialog.hideDialog();
            }

            @Override
            //Handle execution failures//
            public void onFailure(Call<User> call, Throwable throwable) {
                Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                viewDialog.hideDialog();
            }
        });
    }
}
