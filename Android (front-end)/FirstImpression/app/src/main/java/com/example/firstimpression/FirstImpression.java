package com.example.firstimpression;

import android.app.Application;

public class FirstImpression extends Application {

    private User currentUser;
    private String jwtToken;

    public User getCurrentUser() {
        return currentUser;
    }
    public void setCurrentUser(User user) {
        this.currentUser = user;
    }
    public String getToken() { return jwtToken; }
    public void setToken(String token) {
        this.jwtToken = token;
    }
}
